package org.willtanium.whisper.app;

import java.util.concurrent.ExecutionException;

public class App {
    static {
        System.loadLibrary("native-lib");
    }
    private long nativePtr;
    private static App instance;
    public native long initApp();
    private native void setPlayBackDevice(int playBackDeviceId, long nativePtr);
    private native boolean isAAudioSupported(long nativePtr);
    private native boolean setAPI(int apiType, long nativePtr);
    private native void setCaptureDevice(int captureDeviceId, long nativePtr);
    private native void start(long nativePtr);
    private native void stop(long nativePtr);
    public native void shutdownApp(long nativePtr);

    public App() {
        instance = this;
    }

    public void init() {
        nativePtr = initApp();
    }

    public void start() throws ExecutionException {
        if (nativePtr > 0) {
            start(nativePtr);
        } else {
            throw new ExecutionException(new Throwable("Failed to start recorder"));
        }
    }

    public void stop() {
        if (nativePtr > 0) {
            stop(nativePtr);
        }
    }

    public void setRecordingDevice(int recordingDeviceId) {
        if(nativePtr > 0) {
            setCaptureDevice(recordingDeviceId, nativePtr);
        }
    }

    public void setPlaybackDevice(int playbackDeviceId) {
        if(nativePtr > 0) {
            setPlayBackDevice(playbackDeviceId, nativePtr);
        }
    }
    public boolean isAAudioSupported() {
        if(nativePtr > 0) {
            return isAAudioSupported(nativePtr);
        }
        return false;
    }

    public boolean setAPI(int apiType) {
        if(nativePtr > 0) {
            return setAPI(apiType,nativePtr);
        }
        return false;
    }

    public void shutdown(){
        shutdownApp(nativePtr);
        nativePtr = 0;
    }
}
