package org.willtanium.whisper.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.willtanium.whisper.R;


public class AudioDeviceAdapter extends ArrayAdapter<AudioDeviceListEntry> {
    public AudioDeviceAdapter(Context context) {
        super(context, R.layout.audio_devices);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getDropDownView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            rowView = inflater.inflate(R.layout.audio_devices, parent, false);
        }

        TextView deviceName = rowView.findViewById(R.id.device_name);
        AudioDeviceListEntry deviceInfo = getItem(position);
        deviceName.setText(deviceInfo.getName());

        return rowView;
    }
}
