#include <vector>
#include <whisper.h>
#include <logging_macros.h>

Whisper::Whisper() : transport((*new asio::io_service()), false, Whisper::process_incoming_tcp_message, Whisper::process_incoming_audio, this),
                     audio(DEFAULT_OPUS_ENCODER_BITRATE),
                     session_id(0),
                     channel_id(0) {
  version_cb = nullptr;
  audio_cb = nullptr;
  server_sync_cb = nullptr;
  channel_removed_cb = nullptr;
  channel_state_cb = nullptr;
  user_remove_cb = nullptr;
  user_state_cb = nullptr;
  ban_list_cb = nullptr;
  text_message_cb = nullptr;
  permission_denied_cb = nullptr;
  user_query_cb = nullptr;
  context_action_modify_cb = nullptr;
  user_list_cb = nullptr;
  permission_query_cb = nullptr;
  codec_version_cb = nullptr;
  server_config_cb = nullptr;
  suggested_config_cb = nullptr;
  acl_cb = nullptr;
  do_ping = false;
}

int Whisper::connect(std::string host, int port, std::string user, std::string password) {
  do_ping = true;
  return transport.connect(host, port, user, password);
}
void Whisper::disconnect() {
  if (transport.getConnectionState() == ConnectionState::CONNECTED) {
    transport.disconnect();
    do_ping = false;
  }
}
void Whisper::run() {
  std::thread t(&Whisper::periodic_ping, this);
  t.detach();
  transport.run();
}

void Whisper::periodic_ping() {
  do {
    transport.update_ping();
  } while (do_ping);
}

ConnectionState Whisper::get_connection_state() {
  return transport.getConnectionState();
}
void Whisper::send_audio_data(int16_t *pcm_data, int length) {
  uint8_t encoded_data[5000];
  int len = audio.encode_audio_packet(0, pcm_data, length, encoded_data, 5000);
  transport.write_audio_packet(encoded_data, len);
}

void Whisper::send_text_message(std::string message) {
  MumbleProto::TextMessage textMessage;
  textMessage.set_actor(session_id);
  textMessage.add_channel_id(channel_id);
  textMessage.set_message(message);
  uint8_t buffer[MAX_TCP_LENGTH];
  int payload_size = transport.payload_sizer(textMessage.ByteSize(), MessageType::TEXTMESSAGE, buffer);
  textMessage.SerializeToArray(buffer + payload_size, textMessage.ByteSize());
  transport.send_ssl_message(buffer, payload_size + textMessage.ByteSize());
}
void Whisper::join_channel(int channel_id) {
  MumbleProto::UserState userState;
  userState.set_channel_id(channel_id);
  uint8_t buffer[MAX_TCP_LENGTH];
  int payload_size = transport.payload_sizer(userState.ByteSize(), MessageType::USERSTATE, buffer);
  userState.SerializeToArray(buffer + payload_size, userState.ByteSize());
  transport.send_ssl_message(buffer, payload_size + userState.ByteSize());
  this->channel_id = channel_id;
}


bool Whisper::process_incoming_audio(AudioPacketType type, uint8_t *buffer, int length, void *ctx) {
  LOGI("Got %d of encoded audio data", length);
  Whisper *whisper = static_cast<Whisper *>(ctx);
  auto audio_packet = whisper->audio.decode_audio(buffer, length);
  if (type != AudioPacketType::OPUS) {
    LOGW("Incoming audio is not OPUS encoded");
  } else {
    int16_t pcm_data[5000];
    auto status = whisper->audio.decode_opus_payload(audio_packet.audio_data, audio_packet.length, pcm_data, 5000);
  }
  return true;
}

void Whisper::version_call_handler(uint8_t *buffer, int length) {
  MumbleProto::Version version;
  version.ParseFromArray(buffer, length);
  if (version_cb != nullptr) {
    version_cb(version.version() >> 16, version.version() >> 8 & 0xff, version.version() & 0xff, version.release(), version.os(), version.os_version());
  } else {
    LOGW("Version callback not set");
  }
}
void Whisper::server_sync_handler(uint8_t *buffer, int length) {
  MumbleProto::ServerSync serverSync;
  serverSync.ParseFromArray(buffer, length);
  session_id = serverSync.session();
  if (server_sync_cb != nullptr) {
    server_sync_cb(serverSync.welcome_text(), serverSync.session(), serverSync.max_bandwidth(), serverSync.permissions());
  } else {
    LOGW("Server Sync Callback not set");
  }
}
void Whisper::channel_removed_handler(uint8_t *buffer, int length) {
  MumbleProto::ChannelRemove channelRemoved;
  channelRemoved.ParseFromArray(buffer, length);
  if (channel_removed_cb != nullptr) {
    channel_removed_cb(channelRemoved.channel_id());
  } else {
    LOGW("Channel Removed callback not set");
  }
}
void Whisper::channel_state_handler(uint8_t *buffer, int length) {
  MumbleProto::ChannelState channelState;
  channelState.ParseFromArray(buffer, length);
  int32_t channel_id = channelState.has_channel_id() ? channelState.channel_id() : -1;
  int32_t parent = channelState.has_parent() ? channelState.parent() : -1;
  bool temporary = channelState.has_temporary() ? channelState.temporary() : false;
  int position = channelState.has_position() ? channelState.position() : 0;
  std::vector<uint32_t> links;
  std::vector<uint32_t> links_added;
  std::vector<uint32_t> links_removed;
  std::for_each(channelState.links_add().begin(), channelState.links_add().end(), [&](uint32_t link) { links_added.push_back(link); });
  std::for_each(channelState.links().begin(), channelState.links().end(), [&](uint32_t link) { links.push_back(link); });
  std::for_each(channelState.links_remove().begin(), channelState.links_remove().end(), [&](uint32_t link) { links_removed.push_back(link); });
  this->channel_id = channel_id;
  if (channel_state_cb != nullptr) {
    channel_state_cb(channelState.name(), channel_id, parent, channelState.description(), links, links_added, links_removed, temporary, position);
  } else {
    LOGW("Channel State handler not set");
  }
}

void Whisper::user_remove_handler(uint8_t *buffer, int length) {
  MumbleProto::UserRemove userRemove;
  userRemove.ParseFromArray(buffer, length);
  int32_t actor = userRemove.has_actor() ? userRemove.actor() : -1;
  bool ban = userRemove.has_ban() ? userRemove.ban() : false;
  if (user_remove_cb != nullptr) {
    user_remove_cb(userRemove.session(), actor, userRemove.reason(), ban);
  } else {
    LOGW("User Remove Callback not set");
  }
}

void Whisper::user_state_handler(uint8_t *buffer, int length) {
  MumbleProto::UserState userState;
  userState.ParseFromArray(buffer, length);
  // There are far too many things in this structure. Culling to the ones that are probably important
  int32_t session = userState.has_session() ? userState.session() : -1;
  int32_t actor = userState.has_actor() ? userState.actor() : -1;
  int32_t user_id = userState.has_user_id() ? userState.user_id() : -1;
  int32_t channel_id = userState.has_channel_id() ? userState.channel_id() : -1;
  int32_t mute = userState.has_mute() ? userState.mute() : -1;
  int32_t deaf = userState.has_deaf() ? userState.deaf() : -1;
  int32_t suppress = userState.has_suppress() ? userState.suppress() : -1;
  int32_t self_mute = userState.has_self_mute() ? userState.self_mute() : -1;
  int32_t self_deaf = userState.has_self_deaf() ? userState.self_deaf() : -1;
  int32_t priority_speaker = userState.has_priority_speaker() ? userState.priority_speaker() : -1;
  int32_t recording = userState.has_recording() ? userState.recording() : -1;
  if (user_state_cb != nullptr) {
    user_state_cb(session, actor, userState.name(), user_id, channel_id, mute, deaf, suppress, self_mute, self_deaf, userState.comment(), priority_speaker, recording);
  } else {
    LOGW("User State callback not set");
  }
}

void Whisper::banlist_handler(uint8_t *buffer, int length) {
  MumbleProto::BanList banlist;
  banlist.ParseFromArray(buffer, length);
  if (ban_list_cb != nullptr) {
    std::for_each(banlist.bans().begin(), banlist.bans().end(), [&](MumbleProto::BanList::BanEntry ban) {
      ban_list_cb(reinterpret_cast<const uint8_t *>(ban.address().c_str()),
                  ban.address().size(),
                  ban.mask(),
                  ban.name(),
                  ban.hash(),
                  ban.reason(),
                  ban.start(),
                  ban.has_duration() ? ban.duration() : -1);
    });
  } else {
    LOGW("Ban List callback not set");
  }
}

void Whisper::handle_text_messages(uint8_t *buffer, int length) {
  MumbleProto::TextMessage textMessage;
  textMessage.ParseFromArray(buffer, length);
  int32_t actor = textMessage.has_actor() ? textMessage.actor() : -1;
  std::vector<uint32_t> sessions, channel_ids, tree_ids;
  std::for_each(textMessage.session().begin(), textMessage.session().end(), [&](int32_t seesion_id) { sessions.push_back(session_id); });
  std::for_each(textMessage.channel_id().begin(), textMessage.channel_id().end(), [&](int32_t channel_id) { channel_ids.push_back(channel_id); });
  std::for_each(textMessage.tree_id().begin(), textMessage.tree_id().end(), [&](uint32_t tree_id) { tree_ids.push_back(tree_id); });

  if (text_message_cb != nullptr) {
    text_message_cb(actor, sessions, channel_ids, tree_ids, textMessage.message());
  } else {
    LOGW("Text message callback not set");
  }
}

void Whisper::codec_version_handler(uint8_t *buffer, int length) {
  MumbleProto::CodecVersion codecVersion;
  codecVersion.ParseFromArray(buffer, length);
  if (codec_version_cb != nullptr) {
    codec_version_cb(codecVersion.alpha(),
                     codecVersion.beta(),
                     codecVersion.prefer_alpha(),
                     codecVersion.has_opus() ? codecVersion.opus() : false);
  } else {
    LOGW("Codec Version callback not set");
  }
}

void Whisper::server_config_handler(uint8_t *buffer, int length) {
  MumbleProto::ServerConfig serverConfig;
  serverConfig.ParseFromArray(buffer, length);
  uint32_t max_bandwidth = serverConfig.has_max_bandwidth() ? serverConfig.max_bandwidth() : 0;
  uint32_t allow_html = serverConfig.has_allow_html() ? serverConfig.allow_html() : 0;
  uint32_t message_length = serverConfig.has_message_length() ? serverConfig.message_length() : 0;
  uint32_t image_message_length = serverConfig.has_image_message_length() ? serverConfig.image_message_length() : 0;
  if (server_config_cb != nullptr) {
    server_config_cb(max_bandwidth, serverConfig.welcome_text(), allow_html, message_length, image_message_length);
  } else {
    LOGW("Server Config Handler callback not set");
  }
}

void Whisper::permission_denied_handler(uint8_t *buffer, int length) {
  MumbleProto::PermissionDenied permissionDenied;
  permissionDenied.ParseFromArray(buffer, length);
  if (permission_denied_cb != nullptr) {
    permission_denied_cb(permissionDenied.has_permission() ? permissionDenied.permission() : -1,
                         permissionDenied.has_channel_id() ? permissionDenied.channel_id() : -1,
                         permissionDenied.has_session() ? permissionDenied.session() : -1,
                         permissionDenied.has_reason() ? permissionDenied.reason() : "",
                         permissionDenied.type(),
                         permissionDenied.has_name() ? permissionDenied.name() : "");
  } else {
    LOGW("Permission denied callback not set ");
  }

  LOGW("Permission denied handling not implemented");
}
void Whisper::acl_handler(uint8_t *buffer, int length) {
  MumbleProto::ACL acl;
  acl.ParseFromArray(buffer, length);
  if (acl_cb != nullptr) {
    acl_cb(acl.channel_id(), acl.inherit_acls(), acl.query());
  } else {
    LOGW("ACL callback has not been implemented");
  }
  LOGW("ACL handling not implemented");
}
void Whisper::query_users_handler(uint8_t *buffer, int length) {
  LOGW("Query Users not implemented");
}
void Whisper::context_action_modify_handler(uint8_t *buffer, int length) {
  LOGW("Context Action Modify not implemented");
}
void Whisper::context_action_handler(uint8_t *buffer, int length) {
  LOGW("Context Action Not implemented");
}
void Whisper::user_list_handler(uint8_t *buffer, int length) {
  LOGW("User list Not implemented");
}
void Whisper::voice_target_handler(uint8_t *buffer, int length) {
  LOGW("Voice Target not implemented");
}
void Whisper::permission_query_handler(uint8_t *buffer, int length) {
  LOGW("Permission Query not implemented");
}

void Whisper::suggested_config_handler(uint8_t *buffer, int length) {
  LOGW("Suggest config not implemented");
}

bool Whisper::process_incoming_tcp_message(MessageType type, uint8_t *buffer, int length, void *ctx) {
  Whisper *whisper = static_cast<Whisper *>(ctx);
  switch (type) {
  case MessageType::VERSION: {
    whisper->version_call_handler(buffer, length);
  } break;
  case MessageType::SERVERSYNC: {
    whisper->server_sync_handler(buffer, length);
  } break;
  case MessageType::CHANNELREMOVE: {
    whisper->channel_removed_handler(buffer, length);
  } break;
  case MessageType::CHANNELSTATE: {
    whisper->channel_state_handler(buffer, length);
  } break;
  case MessageType::USERSTATE: {
    whisper->user_state_handler(buffer, length);
  } break;
  case MessageType::USERREMOVE: {
    whisper->user_remove_handler(buffer, length);
  } break;
  case MessageType::BANLIST: {
    whisper->banlist_handler(buffer, length);
  } break;
  case MessageType::TEXTMESSAGE: {
    whisper->handle_text_messages(buffer, length);
  } break;
  case MessageType::PERMISSIONDENIED: {
    whisper->permission_denied_handler(buffer, length);
  } break;
  case MessageType::ACL: {
    whisper->acl_handler(buffer, length);
  } break;
  case MessageType::QUERYUSERS: {
    whisper->query_users_handler(buffer, length);
  } break;
  case MessageType::CONTEXTACTIONMODIFY: {
    whisper->context_action_modify_handler(buffer, length);
  } break;
  case MessageType::CONTEXTACTION: {
    whisper->context_action_handler(buffer, length);
  } break;
  case MessageType::USERLIST: {
    whisper->user_list_handler(buffer, length);
  } break;
  case MessageType::VOICETARGET: {
    whisper->voice_target_handler(buffer, length);
  } break;
  case MessageType::PERMISSIONQUERY: {
    whisper->permission_query_handler(buffer, length);
  } break;
  case MessageType::CODECVERSION: {
    whisper->codec_version_handler(buffer, length);
  } break;
  case MessageType::USERSTATS: {
    LOGW("User Stats not implemented");
  } break;
  case MessageType::REQUESTBLOB: {
    LOGW("Request blob not implemented");
  } break;
  case MessageType::SERVERCONFIG: {
    whisper->server_config_handler(buffer, length);
  } break;
  case MessageType::SUGGESTCONFIG: {
    whisper->suggested_config_handler(buffer, length);
  } break;
  }
    return true;
}

Whisper::~Whisper() {
}