//
// Created by willtanium on 25/09/2018.
//

#include <app.h>
#include <logging_macros.h>

App::App() : whisper(), run_app(false), is_audio_online(false), audioHandler() {
    whisper.on_version_cb(
            [&](uint16_t major, uint8_t minor, uint8_t patch, std::string release, std::string os,
                std::string os_version) {
                LOGI("%d:%d:%d REL:%s\n", major, minor, patch, release.c_str());
            });
    whisper.on_codec_version_cb([&](int32_t alpha, int32_t beta, bool prefer_alpha, bool opus) {
        LOGI("ALPHA %d: BETA %d\n", alpha, beta);
        if (opus)
            printf("IS OPUS");
        audioHandler.start();
    });
    whisper.on_audio_cb([&](int target, int session_id, int sequence_number, int16_t *pcm_data,
                             uint32_t pcm_data_size) {
        LOGI("TARGET:%d SESSION ID:%d SEQ-NO:%d PCM-DATA-SIZE:%d\n", target, session_id,
             sequence_number, pcm_data_size);
        audioHandler.writeAudio(pcm_data, pcm_data_size);
    });
    whisper.on_channel_state_cb(
            [&](std::string name, int32_t channel_id, int32_t parent, std::string description,
                std::vector<uint32_t> links,
                std::vector<uint32_t> links_added, std::vector<uint32_t> links_removed,
                bool temporary, int32_t position) {
                LOGI("NAME:%s CHANNEL-ID:%d PARENT:%d DESC:%s\n", name.c_str(), channel_id, parent,
                     description.c_str());
            });
    whisper.on_permission_query_cb([&](int32_t channel_id, uint32_t permissions, int32_t flush) {
        LOGI("CHANNEL-ID:%d\n FLUSH:%d", channel_id, flush);
    });
    whisper.on_user_state_cb([&](int32_t session, int32_t actor, std::string name, int32_t user_id,
                                  int32_t channel_id, int32_t mute, int32_t deaf, int32_t suppress,
                                  int32_t self_mute, int32_t selt_deaf, std::string comment,
                                  int32_t priority_speaker, int32_t recording) {
        LOGI("SESSION:%d ACTOR:%d NAME:%s USER-ID:%d\n", session, actor, name.c_str(), user_id);
    });
    whisper.on_server_config_cb(
            [&](uint32_t max_bandwidth, std::string welcome_text, uint32_t allow_html,
                uint32_t message_length, uint32_t image_message_length) {
                LOGI("MAX-BAND-WIDTH:%d\nWELCOME-TEXT:%s", max_bandwidth, welcome_text.c_str());
            });
    whisper.on_server_sync_cb([&](std::string welcome_text, int32_t session, int32_t max_bandwidth,
                                   int64_t permissions) {
        LOGI("SYNC-DONE\n::::\n ---%s---\nSESSION->%d", welcome_text.c_str(), session);
    });
}


void App::setPlayBackDevice(int playbackDeviceId) {
    audioHandler.setPlaybackDeviceId(playbackDeviceId);
}

void App::setRecordingDevice(int recordingDeviceId) {
    audioHandler.setRecordingDeviceId(recordingDeviceId);
}

bool App::isAAudioSupported() {
    return audioHandler.isAAudioSupported();
}

bool App::setAudioApi(oboe::AudioApi audioApi) {
    return audioHandler.setAudioApi(audioApi);
}

void App::start() {
    audioHandler.setAudioCB(this,[](void *cxt, void *audioData, int32_t numFrames){
        App *app = static_cast<App *>(cxt);
        app->whisper.send_audio_data(static_cast<int16_t *>(audioData), numFrames);
    });

    this->host = std::string("am1.cheapmumble.com");
    this->port = 2007;
    this->user = std::string("mobuzi");
    this->password = std::string("wewilltrythis4communicationnow");
    run_app = true;
    std::thread t_1([this]() {
        try {
            while (run_app) {
                this->whisper.connect(host, port, user, password);
                this->whisper.run();
            }
        }catch(std::exception &e) {
            LOGE("ERROR: %s",e.what());
            std::this_thread::sleep_for(std::chrono::seconds(5));
        }
    });
    t_1.detach();

}

void App::stop() {
    run_app = false;
    whisper.disconnect();
     audioHandler.stop();
}

App::~App() {

}
