#include <audio.h>
#include <array>
#include <var_int.h>
#include <logging_macros.h>

using namespace std::chrono;
static std::chrono::milliseconds RESET_SEQUENCE_NUMBER_INTERVAL(5000);

Audio::Audio(int encoder_bit_rate) : bit_rate(encoder_bit_rate), decoder(nullptr),
                                     encoder(nullptr) {
    int error;
    decoder = opus_decoder_create(SAMPLE_RATE, 1, &error);
    if (error != OPUS_OK) {
        LOGE("Failed to initialize OPUS decoder: %s", opus_strerror(error));
    }
    encoder = opus_encoder_create(SAMPLE_RATE, 1, OPUS_APPLICATION_VOIP, &error);
    if (error != OPUS_OK) {
        LOGE("Failed to initialize OPUS decoder: %s", opus_strerror(error));
    }
    opus_encoder_ctl(encoder, OPUS_SET_VBR(0));
    set_bitrate(encoder_bit_rate);
    reset();
}

AudioPacket Audio::decode_audio(uint8_t *input_buffer, int length) {
    AudioPacket packet;
    packet.type = static_cast<AudioPacketType>((input_buffer[0] & 0xE0) >> 5);
    packet.target = input_buffer[0] & 0x1F;
    std::array<int64_t *, 2> varInts = {&packet.session_id, &packet.sequence_number};
    int dataPointer = 1;
    for (int64_t *val : varInts) {
        VarInt varInt(&input_buffer[dataPointer]);
        *val = varInt.getValue();
        dataPointer += varInt.getEncoded().size();
    }
    packet.audio_data = &input_buffer[dataPointer];
    packet.length = length - dataPointer;

    if (dataPointer >= length) {
        LOGE("invalid incoming audio packet (%d B): header %d B", length, dataPointer);
    }

    LOGD(
            "Received %d B of audio packet, %d B header, %d B payload (target: %d, sessionID: %ld, seq num: %ld).",
            length,
            dataPointer,
            packet.length,
            packet.target,
            packet.session_id,
            packet.sequence_number);
    return packet;
}

std::pair<int, bool>
Audio::decode_opus_payload(uint8_t *buffer, int length, int16_t *pcm_buffer, int pcm_buffer_size) {
    int64_t opusDataLength;

    int dataPointer = 0;
    VarInt varInt(buffer);
    opusDataLength = varInt.getValue();
    dataPointer += varInt.getEncoded().size();

    bool lastPacket = (opusDataLength & 0x2000) != 0;
    opusDataLength = opusDataLength & 0x1fff;

    if (length < opusDataLength + dataPointer) {
        LOGE("Invalid Opus payload {0}, expected Opus data length {1}", length, opusDataLength);
    }

    int outputSize = opus_decode(decoder,
                                 reinterpret_cast<const unsigned char *>(&buffer[dataPointer]),
                                 opusDataLength,
                                 pcm_buffer,
                                 pcm_buffer_size,
                                 0);

    if (outputSize <= 0) {
        LOGE("failed to decode {%d} B of OPUS data: {%d}", length, outputSize);
    }

    LOGD("%d B of Opus data decoded to %d PCM samples, last packet: %d.",
         opusDataLength, outputSize, lastPacket);
    return std::make_pair(outputSize, lastPacket);
}

int
Audio::encode_audio_packet(int target, int16_t *in_pcm_buffer, int in_length, uint8_t *out_buffer,
                           int out_length) {
    std::vector<uint8_t> header;
    const int lastAudioPacketSentInterval = duration_cast<milliseconds>(
            system_clock::now() - last_encoded_at)
            .count();

    if (lastAudioPacketSentInterval > RESET_SEQUENCE_NUMBER_INTERVAL.count() + 1000) {
        LOGD("Last audio packet was sent %d ms ago, resetting encoder.", lastAudioPacketSentInterval);
        reset();
    }

    header.push_back(0x80 | target);
    auto sequence_num_enc = VarInt(outgoing_seq_number).getEncoded();
    header.insert(header.end(), sequence_num_enc.begin(), sequence_num_enc.end());

    uint8_t temp_opus_buffer[1024];
    const int output_size = opus_encode(encoder,
                                        in_pcm_buffer,
                                        in_length,
                                        temp_opus_buffer,
                                        std::min(out_length, 1024));
    if (output_size <= 0) {
        LOGE("Failed to encode %d of PCM data: %s", in_length,
                                  opus_strerror(output_size));
        return 0;
    }

    auto output_size_enc = VarInt(output_size).getEncoded();
    header.insert(header.end(), output_size_enc.begin(), output_size_enc.end());
    LOGI("BEFORE BUFFER PACK");
    memcpy(out_buffer, &header[0], header.size());
    memcpy(out_buffer + header.size(), temp_opus_buffer, output_size);
    LOGI("BUFFER PACK DONE");
    int incrementNumber = 100 * in_length / SAMPLE_RATE;

    outgoing_seq_number += incrementNumber;

    last_encoded_at = std::chrono::system_clock::now();
    LOGI("OPUS ENCODE COMPLETE");
    return output_size + header.size();
}

void Audio::set_bitrate(int bitrate) {
    int error = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(bit_rate));
    if (error != OPUS_OK) {
        LOGE("Failed to set bit rate : {%d}  {%s}", bit_rate,
                                  opus_strerror(error));
    }
}

int Audio::get_bitrate() {
    opus_int32 bitrate;
    int error = opus_encoder_ctl(encoder, OPUS_GET_BITRATE(&bitrate));
    if (error != OPUS_OK) {
        LOGE("Failed to read Opus bitrate: {%s}", opus_strerror(error));
    }
    return bitrate;
}

void Audio::reset() {
    LOGI("RESETTING OPUS");
    int status = opus_encoder_ctl(encoder, OPUS_RESET_STATE, nullptr);
    if (status != OPUS_OK) {
        LOGE("failed to reset encoder: %s", opus_strerror(status));
    }
    outgoing_seq_number = 0;
    LOGI("RESETTING OPUS DONE");
}

Audio::~Audio() {
    if (decoder)
        opus_decoder_destroy(decoder);
    if (encoder)
        opus_encoder_destroy(encoder);
}