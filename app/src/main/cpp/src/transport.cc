#include <map>

#include <sstream>
#include <transport.h>
#include <var_int.h>
#include <vector>
#include <logging_macros.h>


const long CLIENT_VERSION = 0x01020A;
const std::string CLIENT_RELEASE("Whisper");
const std::string CLIENT_OS("OS Unknown");
const std::string CLIENT_OS_VERSION("1");

using namespace std::literals;
using tsc::TaskScheduler;
using tsc::TaskContext;

static std::map<MumbleProto::Reject_RejectType, std::string> rejectMessages = {
        {MumbleProto::Reject_RejectType_None,              "no reason provided"},
        {MumbleProto::Reject_RejectType_WrongVersion,      "wrong version"},
        {MumbleProto::Reject_RejectType_InvalidUsername,   "invalid username"},
        {MumbleProto::Reject_RejectType_WrongUserPW,       "wrong user password"},
        {MumbleProto::Reject_RejectType_WrongServerPW,     "wrong server password"},
        {MumbleProto::Reject_RejectType_UsernameInUse,     "username in use"},
        {MumbleProto::Reject_RejectType_ServerFull,        "server full"},
        {MumbleProto::Reject_RejectType_NoCertificate,     "no certificate provided"},
        {MumbleProto::Reject_RejectType_AuthenticatorFail, "authenticator fail"}};

Transport::Transport(io_service &net_service, bool noUDP, PC_FN fc_cb, PE_AUDIO_FN aud_cb,
                     void *ctx) : net_service(net_service), ssl_context(ssl::context::sslv23),
                                  ssl_socket(net_service, ssl_context), udp_socket(net_service),
                                  no_udp(noUDP), context(ctx),
                                  state(ConnectionState::NOT_CONNECTED),
                                  pc_fn(fc_cb), pe_audio_fn(aud_cb),
                                  data_buffer(std::max(MAX_UDP_LENGTH, MAX_TCP_LENGTH)),
                                  is_initialized(false) {
    pingScheduler.Schedule(5s, [&](TaskContext context) {
        if (state == ConnectionState::CONNECTED) {
            send_ssl_ping();
            if (!no_udp) {
                send_udp_ping();
            }
        }
        context.Repeat();
    });
    ssl_incoming_buffer = new uint8_t[MAX_TCP_LENGTH];
}

void Transport::update_ping() {
    pingScheduler.Update();
}

int Transport::connect(const std::string &host, const int &port, const std::string &user,
                       const std::string &password) {
    this->user = user;
    this->password = password;
    this->host = host;
    this->port = port;
    state = ConnectionState::IN_PROGRESS;
    if (!no_udp) {
        connect_udp();
    }
    connect_tcp();

    return 0;
}

void Transport::do_authentication() {
    MumbleProto::Authenticate authenticate;
    authenticate.set_username(user);
    authenticate.set_password(password);
    authenticate.clear_celt_versions();
    authenticate.clear_tokens();
    authenticate.set_opus(true);
    LOGI("Sending authentication.");
    uint8_t buffer[MAX_TCP_LENGTH];

    int payload_size = payload_sizer(authenticate.ByteSize(), MessageType::AUTHENTICATE, buffer);
    authenticate.SerializeToArray(buffer + payload_size, authenticate.ByteSize());
    send_ssl_message(buffer, payload_size + authenticate.ByteSize());
}

void Transport::send_version() {
    MumbleProto::Version version;
    version.set_version(CLIENT_VERSION);
    version.set_os(CLIENT_OS);
    version.set_release(CLIENT_RELEASE);
    version.set_os_version(CLIENT_OS_VERSION);
    LOGI("Sending version information.");
    uint8_t buffer[MAX_TCP_LENGTH];
    int payload_size = payload_sizer(version.ByteSize(), MessageType::VERSION, buffer);
    version.SerializeToArray(buffer + payload_size, version.ByteSize());
    send_ssl_message(buffer, payload_size + version.ByteSize());
}


void Transport::write_audio_packet(uint8_t *buffer, int length) {
    if (state != ConnectionState::CONNECTED) {
        LOGW("Connection not established.");
        return;
    }
    if (udp_active) {
        LOGI("Sending {} Bytes of audio data via UDP", length);
        send_udp_message(buffer, length);
    } else {
        LOGI("Sending {%d} Bytes of audio data via TCP", length);
        const uint16_t net_udp_tunnel_type = htons(static_cast<uint16_t>(MessageType::UDPTUNNEL));
        const uint32_t net_length = htonl(length);
        uint8_t packet_buffer[MAX_TCP_LENGTH];
        memcpy(packet_buffer, &net_udp_tunnel_type, sizeof(net_udp_tunnel_type));
        memcpy(packet_buffer + sizeof(net_udp_tunnel_type), &net_length, sizeof(net_length));
        memcpy(packet_buffer + sizeof(net_udp_tunnel_type) + sizeof(net_length), buffer, length);
        // send_ssl_message(packet_buffer, length + sizeof(net_udp_tunnel_type) + sizeof(net_length));
        if (length + sizeof(net_udp_tunnel_type) + sizeof(net_length) > MAX_TCP_LENGTH) {
            LOGW("Sending {%d} of data via SSL, Maximum allowed is {%d}", length,
                 MAX_TCP_LENGTH);
        }
        memcpy(&data_buffer[0], packet_buffer,
               length + sizeof(net_udp_tunnel_type) + sizeof(net_length));
        LOGD("Sending {%d} Bytes of data asynchronously", length);
        //async_write(AsyncWriteStream &s, const ConstBufferSequence &buffers, WriteHandler &&handler)
        async_write(ssl_socket, asio::buffer(&data_buffer[0], length),
                    [&](asio::error_code ec, size_t bytes_transferred) {
                        data_buffer.clear();
                        LOGD("Send %d bytes of data", bytes_transferred);
                        if (ec)
                            LOGW("ASYNC SSL send failed: %s", ec.message().c_str());
                    });
    }
}

int Transport::payload_sizer(int payload_buffer_length, MessageType type, uint8_t *buffer) {
    const uint16_t type_network = htons(static_cast<uint16_t>(type));
    const uint32_t size_network = htonl(payload_buffer_length);
    const int length = sizeof(type_network) + sizeof(size_network) + payload_buffer_length;
    memcpy(buffer, &type_network, sizeof(type_network));
    memcpy(buffer + sizeof(type_network), &size_network, sizeof(size_network));
    return sizeof(type_network) + sizeof(size_network);
}

void Transport::send_udp_ping() {
    if (state == ConnectionState::NOT_CONNECTED) {
        LOGD("State changed to NOT_CONNECTED, skipping UDP ping.");
        return;
    }
    std::vector<uint8_t> message;
    auto timestamp = VarInt(time(nullptr)).getEncoded();
    message.insert(message.end(), timestamp.begin(), timestamp.end());
    send_udp_message(&message[0], timestamp.size());
}

void Transport::send_ssl_ping() {
    MumbleProto::Ping ping;
    ping.set_timestamp(std::time(nullptr));

    uint8_t buffer[MAX_TCP_LENGTH];
    int payload_size = payload_sizer(ping.ByteSize(), MessageType::PING, buffer);
    ping.SerializeToArray(buffer + payload_size, ping.ByteSize());
    send_ssl_message(buffer, payload_size + ping.ByteSize());
}

void Transport::process_encoded_audio_packet(uint8_t *buffer, int length) {
    AudioPacketType type = static_cast<AudioPacketType>((buffer[0] & 0xE0) >> 5);
    switch (type) {
        case AudioPacketType::CELT_Alpha:
        case AudioPacketType::Speex:
        case AudioPacketType::CELT_Beta:
        case AudioPacketType::OPUS:
            pe_audio_fn(type, buffer, length, context);
            break;
        case AudioPacketType::Ping:
            break;
        default:
            LOGE("Not recognized audio type: %d.", buffer[0]);
    }
}

void Transport::send_udp_message(uint8_t *buffer, int length) {
    if (length > MAX_UDP_LENGTH - 4) {
        LOGE("maximum allowed data length is {%d}", (MAX_UDP_LENGTH - 4));
        return;
    }
    char encrypted_buffer[MAX_UDP_LENGTH + 4];
    const int encrypted_msg_length = length + 4;
    crypt.encrypt(buffer, (unsigned char *) encrypted_buffer, length);

    udp_socket.async_send_to(asio::buffer(encrypted_buffer, encrypted_msg_length),
                             udp_receiver_endpoint,
                             [&](asio::error_code ec, size_t bytes_transferred) {
                                 if (!ec && bytes_transferred > 0)
                                     LOGD(
                                             "UDP Async write %d bytes transferred",
                                             bytes_transferred);
                                 else {
                                     LOGW("UDP Async Write error: %s", ec.message().c_str());
                                 }
                             });
}

void Transport::send_ssl_message(uint8_t *buffer, int length) {
    if (length > MAX_TCP_LENGTH) {
        LOGW("Sending {%d} of data via SSL, Maximum allowed is %d", length, MAX_TCP_LENGTH);
    }

    LOGD("Sending {%d} Bytes of data asynchronously", length);
    try {
        write(ssl_socket, asio::buffer(buffer, length));
    } catch (asio::error_code ec) {
        LOGE("SSL -Write Error: %s", ec.message().c_str());
    }
}

void Transport::process_message_internal(MessageType type, uint8_t *buffer, int length) {
    switch (type) {
        case MessageType::UDPTUNNEL: {
            LOGD("Received {%d} B of encoded audio data via TCP.", length);
            process_encoded_audio_packet(buffer, length);
        }
            break;
        case MessageType::AUTHENTICATE: {
            LOGW("Authenticate message received after authenticated.");
        }
            break;
        case MessageType::PING: {
            MumbleProto::Ping ping;
            ping.ParseFromArray(buffer, length);
            std::stringstream log;
            log << "Received ping.";
            if (ping.has_good()) {
                log << " good: " << ping.good();
            }
            if (ping.has_late()) {
                log << " late: " << ping.late();
            }
            if (ping.has_lost()) {
                log << " lost: " << ping.lost();
            }
            if (ping.has_tcp_ping_avg()) {
                log << " TCP avg: " << ping.tcp_ping_avg() << " ms";
            }
            if (ping.has_udp_ping_avg()) {
                log << " UDP avg: " << ping.udp_ping_avg() << " ms";
            }
            LOGD("ERROR: %s", log.str().c_str());
        }
            break;
        case MessageType::REJECT: {
            MumbleProto::Reject reject;
            reject.ParseFromArray(buffer, length);

            std::stringstream errorMesg;
            errorMesg << "failed to authenticate";

            if (reject.has_type()) {
                errorMesg << ": " << rejectMessages.at(reject.type());
            }

            if (reject.has_reason()) {
                errorMesg << ", reason: " << reject.reason();
            }

            LOGE("ERROR:%s", errorMesg.str().c_str());
        }
            break;
        case MessageType::SERVERSYNC: {
            state = ConnectionState::CONNECTED;

            LOGD("SERVERSYNC. Calling external ProcessControlMessageFunction.");
            pc_fn(type, buffer, length, context);
        }
            break;
        case MessageType::CRYPTSETUP: {
            if (!no_udp) {
                MumbleProto::CryptSetup cryptsetup;
                cryptsetup.ParseFromArray(buffer, length);

                if (cryptsetup.client_nonce().length() != AES_BLOCK_SIZE or
                    cryptsetup.server_nonce().length() != AES_BLOCK_SIZE or
                    cryptsetup.key().length() != AES_BLOCK_SIZE) {
                    LOGE("one of cryptographic parameters has invalid length");
                }

                crypt.setKey(
                        reinterpret_cast<const unsigned char *>(cryptsetup.key().c_str()),
                        reinterpret_cast<const unsigned char *>(cryptsetup.client_nonce().c_str()),
                        reinterpret_cast<const unsigned char *>(cryptsetup.server_nonce().c_str()));

                if (not crypt.isValid()) {
                    LOGE("crypt setup data not valid");
                }

                LOGI("Set up cryptography for UDP transport. Sending UDP ping.");

                send_udp_ping();

            } else {
                LOGI("Ignoring crypt setup message, because UDP is disabled.");
            }
        }
            break;
        default: {
            LOGD("Calling external ProcessControlMessageFunction.");
            pc_fn(type, buffer, length, context);
        }
            break;
    }
}

void Transport::do_receive_udp() {
    if (state == ConnectionState::NOT_CONNECTED) {
        return;
    }
    udp_socket.async_receive_from(asio::buffer(udp_incoming_buffer, MAX_UDP_LENGTH),
                                  udp_receiver_endpoint,
                                  [&](asio::error_code ec, size_t bytes_transferred) {
                                      if (!ec && bytes_transferred > 0) {
                                          LOGI("Received {} Bytes", bytes_transferred);
                                          if (!crypt.isValid()) {
                                              LOGW("UDP packet received before crypt setup");
                                              return;
                                          }
                                          if (udp_active == false) udp_active = true;
                                          uint8_t plain_buffer[1024];
                                          const int plain_buffer_length = bytes_transferred - 4;
                                          if (!crypt.decrypt(udp_incoming_buffer, plain_buffer,
                                                             bytes_transferred)) {
                                              LOGW("UDP de-crypt failed");
                                          } else {
                                              process_encoded_audio_packet(plain_buffer,
                                                                           plain_buffer_length);
                                          }
                                          do_receive_udp();
                                      } else {
                                          LOGW("UDP state %s", ec.message().c_str());
                                      }
                                  });
}

void Transport::do_receive_ssl() {
    if (state == ConnectionState::NOT_CONNECTED) {
        return;
    }

    async_read(ssl_socket, asio::buffer(ssl_incoming_buffer, MAX_TCP_LENGTH),
               [&](const asio::error_code &ec, size_t bytes_transferred) -> size_t {
                   if (bytes_transferred < 6) {
                       return 6 - bytes_transferred;
                   }

                   const int payload_size = ntohl(
                           *reinterpret_cast<uint32_t *>(ssl_incoming_buffer + 2));
                   const int whole_message_length = payload_size + 6;
                   size_t remaining = whole_message_length - bytes_transferred;
                   remaining = std::max(remaining, (size_t) 0);
                   if (whole_message_length > MAX_TCP_LENGTH) {
                       LOGW(
                               "This message size {%d} is bigger than the max allowed size {%d}",
                               whole_message_length, MAX_TCP_LENGTH);
                   }
                   return remaining;
               },
               [&](asio::error_code ec, size_t bytes_transferred) {
                   if (!ec && bytes_transferred > 0) {
                       int message_type = ntohs(*reinterpret_cast<uint16_t *>(ssl_incoming_buffer));
                       LOGD("Received %d bytes of data (%d data) payload type %d",
                            bytes_transferred, bytes_transferred - 6, message_type);
                       process_message_internal(static_cast<MessageType>(message_type),
                                                &ssl_incoming_buffer[6], bytes_transferred - 6);
                       do_receive_ssl();
                   } else {
                       LOGE("SSL Receive error: %s Bytes transferred: %d ",
                            ec.message().c_str(), bytes_transferred);
                   }
               });
}

void Transport::disconnect() {
    if (state == ConnectionState::CONNECTED) {
        asio::error_code ec;
        ssl_socket.shutdown(ec);
        if (ec) {
            LOGE("SHUTDOWN ERROR -> %s", ec.message().c_str());
        }
        ssl_socket.lowest_layer().shutdown(tcp::socket::shutdown_both, ec);
        if (ec) {
            LOGE("SHUTDOWN ERROR -> %s", ec.message().c_str());
        }
        udp_socket.close(ec);
        if (ec) {
            LOGE("SHUTDOWN ERROR -> %s", ec.message().c_str());
        }
        state = ConnectionState::NOT_CONNECTED;
    }
}

void Transport::connect_udp() {
    try {
        LOGI("MAKING UDP CONNECTION");
        ip::udp::resolver resolver_udp(net_service);
        ip::udp::resolver::query query_udp(ip::udp::v4(), host, std::to_string(port));
        LOGI("UDP CONN: HOST:{%s} PORT:{%d}", host.c_str(), port);
        udp_receiver_endpoint = *resolver_udp.resolve(query_udp);
        udp_socket.open(ip::udp::v4());
        LOGI("UDP CONNECTION CREATED");
        do_receive_udp();
    } catch (std::exception &e) {
        LOGE("ERROR:%s", e.what());
    }
}

void Transport::connect_tcp() {
    ssl_socket.set_verify_mode(asio::ssl::verify_peer);
    ssl_socket.set_verify_callback([&](bool pre_verified, asio::ssl::verify_context &ctx) {
        return true;
    });
    try {
        ip::tcp::resolver resolve_tcp(net_service);
        ip::tcp::resolver::query query_tcp(host, std::to_string(port));
        async_connect(ssl_socket.lowest_layer(), resolve_tcp.resolve(query_tcp),
                      [&](asio::error_code ec, tcp::endpoint end_point) {
                          if (!ec) {
                              ssl_socket.async_handshake(asio::ssl::stream_base::client,
                                                         [&](asio::error_code ec) {
                                                             do_receive_ssl();
                                                             send_version();
                                                             do_authentication();
                                                         });
                          } else {
                              LOGE("A-SYNC -CONNECTION FAILED %s", ec.message().c_str());
                          }
                      });
    } catch (std::exception &e) {
        LOGE("ERROR:%s", e.what());
    }
}

Transport::~Transport() {
    delete[] ssl_incoming_buffer;
    disconnect();
}