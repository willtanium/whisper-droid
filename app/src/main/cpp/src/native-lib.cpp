#include <jni.h>
#include <app.h>


static const char *ACTIVITY = "org/willtanium/whisper/app/App";
static const int kOboeApiAAudio = 0;
static const int kOboeApiOpenSLES = 1;
static JNIEnv *global;
static JavaVM *g_vm;
static jclass cls;

extern "C"
JNIEXPORT jlong JNICALL
Java_org_willtanium_whisper_app_App_initApp(JNIEnv *env, jobject instance) {
    App *app = new(std::nothrow) App();
    return (jlong) app;
}

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    g_vm = vm;
    if (vm->GetEnv((void **) (&global), JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }
    cls = reinterpret_cast<jclass>(global->NewGlobalRef(global->FindClass(ACTIVITY)));
    return JNI_VERSION_1_6;
}

extern "C"
JNIEXPORT void JNICALL
Java_org_willtanium_whisper_app_App_setPlayBackDevice(JNIEnv *env, jobject instance,
                                                      jint playBackDeviceId, jlong nativePtr) {

    App *app = reinterpret_cast<App *>(nativePtr);
    app->setPlayBackDevice(playBackDeviceId);

}

extern "C"
JNIEXPORT jboolean JNICALL
Java_org_willtanium_whisper_app_App_isAAudioSupported(JNIEnv *env, jobject instance,
                                                      jlong nativePtr) {
    return static_cast<jboolean>(((App *) reinterpret_cast<App *>(nativePtr))->isAAudioSupported());
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_org_willtanium_whisper_app_App_setAPI(JNIEnv *env, jobject instance, jint apiType,
                                           jlong nativePtr) {
    App *app = reinterpret_cast<App *>(nativePtr);
    oboe::AudioApi audioApi;
    switch (apiType) {
        case kOboeApiAAudio:
            audioApi = oboe::AudioApi::AAudio;
            break;
        case kOboeApiOpenSLES:
            audioApi = oboe::AudioApi::OpenSLES;
            break;
        default:
            //LOGE("Unknown API selection to setAPI() %d", apiType);
            return JNI_FALSE;
    }
    return static_cast<jboolean >(app->setAudioApi(audioApi) ? JNI_TRUE : JNI_FALSE);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_willtanium_whisper_app_App_setCaptureDevice(JNIEnv *env, jobject instance,
                                                     jint captureDeviceId, jlong nativePtr) {
    App *app = reinterpret_cast<App *>(nativePtr);
    app->setRecordingDevice(captureDeviceId);
}


extern "C"
JNIEXPORT void JNICALL
Java_org_willtanium_whisper_app_App_start(JNIEnv *env, jobject instance, jlong nativePtr) {
    App *app = reinterpret_cast<App *>(nativePtr);
    app->start();
}

extern "C"
JNIEXPORT void JNICALL
Java_org_willtanium_whisper_app_App_stop(JNIEnv *env, jobject instance, jlong nativePtr) {
    App *app = reinterpret_cast<App *>(nativePtr);
    app->stop();
}

extern "C"
JNIEXPORT void JNICALL
Java_org_willtanium_whisper_app_App_shutdownApp(JNIEnv *env, jobject instance, jlong nativePtr) {
    delete (App *)nativePtr;
}