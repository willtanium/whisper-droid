#ifndef WHISPER_H
#define WHISPER_H
#include <enums.h>
#include <functional>
#include <string>
#include <transport.h>
#include <audio.h>

typedef std::function<void(uint16_t major, uint8_t minor, uint8_t patch, std::string release, std::string os , std::string os_version)> VERSION_CB;
typedef std::function<void(int target, int session_id, int sequence_number, int16_t *pcm_data, uint32_t pcm_data_size)> AUDIO_CB;
typedef std::function<void(std::string welcome_text, int32_t session, int32_t max_bandwidth, int64_t permissions)> SERVER_SYNC_CB;
typedef std::function<void(uint32_t channel_id)> CHANNEL_REMOVED_CB;
typedef std::function<void(std::string name, int32_t channel_id, int32_t parent, std::string description, std::vector<uint32_t> links,
                           std::vector<uint32_t> links_added, std::vector<uint32_t> links_removed, bool temporary, int32_t position)> CHANNEL_STATE_CB;
typedef std::function<void(uint32_t session, int32_t actor, std::string reason, bool ban)> USER_REMOVE_CB;
typedef std::function<void(int32_t session, int32_t actor, std::string name, int32_t user_id, int32_t channel_id, int32_t mute, int32_t deaf, int32_t suppress,
                           int32_t self_mute, int32_t selt_deaf, std::string comment, int32_t priority_speaker, int32_t recording)> USER_STATE_CB;
typedef  std::function<void(const uint8_t *ip_data, uint32_t ip_data_size, uint32_t mask, std::string name, std::string hash, std::string reason,
                            std::string start, int32_t duration)> BAN_LIST_CB;
typedef std::function<void(uint32_t actor, std::vector<uint32_t> sessions, std::vector<uint32_t> channel_ids, std::vector<uint32_t> tree_ids,
                           std::string message)> TEXT_MESSAGE_CB;
typedef std::function<void(int32_t permission, int32_t channel_id, int32_t session, std::string reason, int32_t deny_type, std::string name)> PERMISSION_DENIED_CB;
typedef std::function<void(int32_t number_of_ids, uint32_t *ids, uint32_t number_of_names, std::string *names)> USER_QUERY_CB;
typedef std::function<void(std::string action, std::string text, uint32_t context, uint32_t operation)> CONTEXT_ACTION_MODIFY_CB;
typedef std::function<void(uint32_t user_id, std::string name, std::string last_seen, int32_t last_channel)> USER_LIST_CB;
typedef std::function<void(int32_t channel_id, uint32_t permissions, int32_t flush)> PERMISSION_QUERY_CB;
typedef std::function<void(int32_t alpha, int32_t beta, bool prefer_alpha, bool opus)> CODEC_VERSION_CB;
typedef std::function<void(uint32_t max_bandwidth, std::string welcome_text, uint32_t allow_html, uint32_t message_length, uint32_t image_message_length)> SERVER_CONFIG_CB;
typedef std::function<void(uint32_t channel_id, bool inherit_acls, bool query)> ACL_CB;
typedef std::function<void(uint32_t version, uint32_t positional, uint32_t push_to_talk)> SUGGESTED_CONFIG_CB;
class Whisper {
public:
  Whisper();
  ~Whisper();
  int connect(std::string host, int port, std::string user, std::string password);
  void disconnect();
  void run();
  ConnectionState get_connection_state();
  void send_audio_data(int16_t *pcm_data, int length);
  void send_text_message(std::string message);
  void join_channel(int channel_id);
  
  // CALLBACK SETTERS
  void on_version_cb(VERSION_CB cb) { version_cb=cb;}
  void on_audio_cb(AUDIO_CB cb) { audio_cb=cb;}
  void on_server_sync_cb(SERVER_SYNC_CB cb) {server_sync_cb=cb;}
  void on_channel_removed_cb(CHANNEL_REMOVED_CB cb) {channel_removed_cb=cb;}
  void on_channel_state_cb(CHANNEL_STATE_CB cb) {channel_state_cb=cb;}
  void on_user_remove_cb(USER_REMOVE_CB cb) {user_remove_cb=cb;}
  void on_user_state_cb(USER_STATE_CB cb) {user_state_cb=cb;}
  void on_ban_list_cb(BAN_LIST_CB cb) {ban_list_cb=cb;}
  void on_text_message_cb(TEXT_MESSAGE_CB cb) {text_message_cb=cb;}
  void on_permission_denied_cb(PERMISSION_DENIED_CB cb) {permission_denied_cb=cb;}
  void on_user_query_cb(USER_QUERY_CB cb) {user_query_cb=cb;}
  void on_context_action_modified_cb(CONTEXT_ACTION_MODIFY_CB cb) {context_action_modify_cb=cb;}
  void on_user_list_cb(USER_LIST_CB cb) {user_list_cb=cb;}
  void on_permission_query_cb(PERMISSION_QUERY_CB cb) {permission_query_cb=cb;}
  void on_codec_version_cb(CODEC_VERSION_CB cb) {codec_version_cb=cb;}
  void on_server_config_cb(SERVER_CONFIG_CB cb) {server_config_cb=cb;}
  void on_suggested_config_cb(SUGGESTED_CONFIG_CB cb) {suggested_config_cb=cb;}
  void on_acl_cb(ACL_CB cb) {acl_cb=cb;}
private:
  static bool process_incoming_audio(AudioPacketType type, uint8_t *buffer, int length, void *ctx);
  static bool process_incoming_tcp_message(MessageType type, uint8_t *buffer, int length, void *ctx);
  void version_call_handler(uint8_t *buffer, int length);
  void server_sync_handler(uint8_t *buffer, int length);
  void channel_removed_handler(uint8_t *buffer, int length);
  void channel_state_handler(uint8_t *buffer, int length);
  void user_remove_handler(uint8_t *buffer, int length);
  void user_state_handler(uint8_t *buffer, int length);
  void banlist_handler(uint8_t *buffer, int length);
  void handle_text_messages(uint8_t *buffer, int length);
  void codec_version_handler(uint8_t *buffer, int length);
  void server_config_handler(uint8_t *buffer, int length);
  void permission_denied_handler(uint8_t *buffer, int length);
  void acl_handler(uint8_t *buffer, int length);
  void query_users_handler(uint8_t *buffer, int length);
  void context_action_modify_handler(uint8_t *buffer, int length);
  void context_action_handler(uint8_t *buffer, int length);
  void user_list_handler(uint8_t *buffer, int length);
  void voice_target_handler(uint8_t *buffer, int length);
  void permission_query_handler(uint8_t *buffer, int length);
  void suggested_config_handler(uint8_t *buffer, int length);
  void periodic_ping();
  Transport transport;
  Audio audio;
  int channel_id;
  int session_id;
  bool do_ping;
  
  // CALLBACKS
  VERSION_CB version_cb;
  AUDIO_CB audio_cb;
  SERVER_SYNC_CB server_sync_cb;
  CHANNEL_REMOVED_CB channel_removed_cb;
  CHANNEL_STATE_CB channel_state_cb;
  USER_REMOVE_CB user_remove_cb;
  USER_STATE_CB user_state_cb;
  BAN_LIST_CB ban_list_cb;
  TEXT_MESSAGE_CB text_message_cb;
  PERMISSION_DENIED_CB permission_denied_cb;
  USER_QUERY_CB user_query_cb;
  CONTEXT_ACTION_MODIFY_CB context_action_modify_cb;
  USER_LIST_CB user_list_cb;
  PERMISSION_QUERY_CB permission_query_cb;
  CODEC_VERSION_CB codec_version_cb;
  SERVER_CONFIG_CB server_config_cb;
  SUGGESTED_CONFIG_CB suggested_config_cb;
  ACL_CB acl_cb;
};
#endif // WHISPER_H