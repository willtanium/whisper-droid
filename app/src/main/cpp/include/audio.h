#ifndef AUDIO_H
#define AUDIO_H
#include <enums.h>
#include <opus.h>
#include <stdint.h>
#include <map>
#include <chrono>
const int SAMPLE_RATE = 48000;
const int DEFAULT_OPUS_ENCODER_BITRATE = 16000;
struct AudioPacket {
  AudioPacketType type;
  int target;
  int64_t session_id;
  int64_t sequence_number;
  uint8_t *audio_data;
  int length;
};
class Audio {
public:
  Audio(int encoder_bit_rate = DEFAULT_OPUS_ENCODER_BITRATE);
  ~Audio();
  AudioPacket decode_audio(uint8_t *input_buffer, int length);
  std::pair<int,bool> decode_opus_payload(uint8_t *buffer, int length, int16_t *pcm_buffer, int pcm_buffer_size);
  int encode_audio_packet(int target, int16_t *in_pcm_buffer, int int_length, uint8_t *out_buffer, int out_length);
  void set_bitrate(int bitrate);
  int get_bitrate();
  void reset();
private:
  OpusDecoder *decoder;
  OpusEncoder *encoder;
  int bit_rate;
  std::chrono::time_point<std::chrono::system_clock> last_encoded_at;
  int64_t outgoing_seq_number;
};

#endif // AUDIO_H