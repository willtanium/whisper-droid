//
// Created by willtanium on 28/09/2018.
//

#ifndef WHISPER_AUDIO_HANDLER_H
#define WHISPER_AUDIO_HANDLER_H

#include <oboe/Oboe.h>
#include <transport.h>

typedef void(*AudioCB)(void *cxt, void *audioData, int32_t numFrames);
class AudioHandler: public oboe::AudioStreamCallback {
public:
    AudioHandler();
    ~AudioHandler();
    void setRecordingDeviceId(int32_t deviceId);
    void setPlaybackDeviceId(int32_t deviceId);
    void setEffectOn(bool isOn);
    oboe::DataCallbackResult onAudioReady(oboe::AudioStream *oboeStream,
                                          void *audioData, int32_t numFrames);
    void onErrorBeforeClose(oboe::AudioStream *oboeStream, oboe::Result error);
    void onErrorAfterClose(oboe::AudioStream *oboeStream, oboe::Result error);
    bool setAudioApi(oboe::AudioApi audioApi);
    bool isAAudioSupported(void);
    void writeAudio(void *audioData, int32_t numFrames);
    void setAudioCB(void *cxt, AudioCB audio_cb);
    void start();
    void stop();

private:
    void *cxt;
    bool mIsEffectOn = false;
    uint64_t mProcessedFrameCount = 0;
    uint64_t mSystemStartupFrames = 0;
    int32_t mRecordingDeviceId = oboe::kUnspecified;
    int32_t mPlaybackDeviceId = oboe::kUnspecified;
    oboe::AudioFormat mFormat = oboe::AudioFormat::I16;
    int32_t mSampleRate = oboe::kUnspecified;
    int32_t mInputChannelCount = oboe::ChannelCount::Stereo;
    int32_t mOutputChannelCount = oboe::ChannelCount::Stereo;
    oboe::AudioStream *mRecordingStream = nullptr;
    oboe::AudioStream *mPlayStream = nullptr;
    std::mutex mRestartingLock;
    oboe::AudioApi mAudioApi = oboe::AudioApi::AAudio;
    AudioCB audio_cb;
    void openRecordingStream();
    void openPlaybackStream();
    void startStream(oboe::AudioStream *stream);
    void stopStream(oboe::AudioStream *stream);
    void closeStream(oboe::AudioStream *stream);
    void openAllStreams();
    void closeAllStreams();
    void restartStreams();
    oboe::AudioStreamBuilder *setupCommonStreamParameters(oboe::AudioStreamBuilder *builder);
    oboe::AudioStreamBuilder *setupRecordingStreamParameters(oboe::AudioStreamBuilder *builder);
    oboe::AudioStreamBuilder *setupPlaybackStreamParameters(oboe::AudioStreamBuilder *builder);
    void warnIfNotLowLatency(oboe::AudioStream *stream);
};
#endif //WHISPER_AUDIO_HANDLER_H
