//
// Created by willtanium on 25/09/2018.
//

#ifndef WHISPER_APP_H
#define WHISPER_APP_H

#include <whisper.h>
#include <audio_handler.h>
class App {

public:
    const int kOboeApiAAudio = 0;
    const int kOboeApiOpenSLES = 1;
    App();
    ~App();
    void start();
    void stop();
    void setPlayBackDevice(int playbackDeviceId);
    void setRecordingDevice(int recordingDeviceId);
    bool isAAudioSupported();
    bool setAudioApi(oboe::AudioApi);
private:
    Whisper whisper;
    bool run_app;
    bool is_audio_online;
    AudioHandler audioHandler;
    std::string host;
    std::string user;
    std::string password;
    int port;
};
#endif //WHISPER_APP_H
